//  ========================================================================
//  COSC363: Computer Graphics (2015);  University of Canterbury.
//
//  FILE NAME: timers.cpp
//  ========================================================================
#ifndef TIMERS_H
#define TIMERS_H
#include <iostream>
#include <GL/freeglut.h>
#include <cmath>


// Angluar position of the train
extern float theta;

//Variables for PersonTimer
//Direction of body parts motion
extern bool isForward;
//Current angle of persons motion cycle
extern float personCycleAngle;
//Current angular position of a person
extern float personAngularPos;

//current facing angle of person in rectangle walk pattern
extern float facingAngle;
//Step size of rectagle walk
extern float step;
//Current position of person in rectagle walk,
extern float person2PosX;
extern float person2PosZ;

//Default timer value
extern int timer_value;

//Camera attached to front of train true/false
extern bool isCameraOnTrain;

//Status of sginaling light
extern bool isSignalRed;

extern int arms_theta;
extern int arms_theta;
extern bool isIncreasing;
extern bool trig_arms;

/*Timer for dictating the motion of the train.*/
void myTimer(int value);




/*Timmer for dictating the motion and position of a person moving in a
circle inside the train track area.*/
void personTimer1(int value);


/*function for dictating the position of a person moving in a rectangular
pattern.
param: rectangle[] - Dimensions of rectangle, current position, current angle,
and step size.
{x, z, xSize, zSize, anglef, step}

return:			   the current position {x, z, anglef}.
*/
void rectangleWalk(float rectangle[], float* position);

/*Timer for dictating the motion of a person moving in a rectangular pattern*/
void personTimer2(int value);

/*Timmer for dictating the colour of the signalling lights between red and
green.
*/
void signalTimmer(int value);


/*Timer for dictaing the motion of the barrier arms when a train is near*/
void arms_timer(int value);

#endif
