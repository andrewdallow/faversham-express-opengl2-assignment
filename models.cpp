#include "models.h"

GLUquadric *q;
//Object textures
GLuint txId[8];

//-- Ground Plane --------------------------------------------------------
void floor()
{
	float white[4] = { 1., 1., 1., 1. };
	float black[4] = { 0 };
	glColor4f(0.15f, 0.25f, 0.07f, 1.0f);
	glNormal3f(0.0, 1.0, 0.0);
	//No specular reflections from the floor.
	glMaterialfv(GL_FRONT, GL_SPECULAR, black);

	//The floor is made up of several tiny squares on a 200x200 grid. 
	//Each square has a unit size.
	glBegin(GL_QUADS);
	for (int i = -200; i < 200; i++)
	{
		for (int j = -200; j < 200; j++)
		{
			glVertex3f(i, 0.0, j);
			glVertex3f(i, 0.0, j + 1);
			glVertex3f(i + 1, 0.0, j + 1);
			glVertex3f(i + 1, 0.0, j);
		}
	}
	glEnd();

	//Enable specular reflections for remaining objects
	glMaterialfv(GL_FRONT, GL_SPECULAR, white);
}

//------- Rail Track ----------------------------------------------------
// A single circular track of specified radius
void track(float radius)
{
	float angle1, angle2, ca1, sa1, ca2, sa2;
	float x1, z1, x2, z2, x3, z3, x4, z4;  //four points of a quad
	float toRad = 3.14159265f / 180.0f;  //Conversion from degrees to radians

	glBegin(GL_QUADS);
	for (int i = 0; i < 360; i += 5)    //5 deg intervals
	{
		angle1 = i * toRad;       //Computation of angles, cos, sin etc
		angle2 = (i + 5) * toRad;
		ca1 = cos(angle1); ca2 = cos(angle2);
		sa1 = sin(angle1); sa2 = sin(angle2);
		x1 = (radius - 0.5f)*sa1; z1 = (radius - 0.5f)*ca1;
		x2 = (radius + 0.5f)*sa1; z2 = (radius + 0.5f)*ca1;
		x3 = (radius + 0.5f)*sa2; z3 = (radius + 0.5f)*ca2;
		x4 = (radius - 0.5f)*sa2; z4 = (radius - 0.5f)*ca2;

		glColor4f(0.54f, 0.52f, 0.51f, 1.0f);
		glNormal3f(0., 1., 0.);       //Quad 1 facing up
		glVertex3f(x1, 1.0, z1);
		glVertex3f(x2, 1.0, z2);
		glVertex3f(x3, 1.0, z3);
		glVertex3f(x4, 1.0, z4);

		glNormal3f(-sa1, 0.0, -ca1);   //Quad 2 facing inward
		glVertex3f(x1, 0.0, z1);
		glVertex3f(x1, 1.0, z1);
		glNormal3f(-sa2, 0.0, -ca2);
		glVertex3f(x4, 1.0, z4);
		glVertex3f(x4, 0.0, z4);

		glNormal3f(sa1, 0.0, ca1);   //Quad 3 facing outward
		glVertex3f(x2, 1.0, z2);
		glVertex3f(x2, 0.0, z2);
		glNormal3f(sa2, 0.0, ca2);
		glVertex3f(x3, 0.0, z3);
		glVertex3f(x3, 1.0, z3);
	}
	glEnd();
}

//-------- Tracks  ----------------------------------------------------
void tracks()
{
	glColor4f(0.0f, 0.0f, 0.3f, 1.0f);
	track(115.0f);   //Inner track has radius 115 units
	track(125.0f);   //Outer track has radius 125 units
}


//--Draws a character model constructed using GLUT object ------------------
void drawPerson()
{
	glColor3f(1.0f, 0.78f, 0.06f);		//Head
	glPushMatrix();
	glTranslatef(0, 7.7, 0);
	glutSolidCube(1.4);
	glPopMatrix();

	glColor3f(1.0f, 0.f, 0.f);			//Torso
	glPushMatrix();
	glTranslatef(0, 5.5, 0);
	glScalef(3.f, 3.f, 1.4f);
	glutSolidCube(1);
	glPopMatrix();

	glColor3f(0., 0., 1.);			//Right leg
	glPushMatrix();
	glTranslatef(-0.8, 4, 0);
	glRotatef(-personCycleAngle, 1, 0, 0);
	glTranslatef(0.8, -4, 0);
	glTranslatef(-0.8, 2.2, 0);
	glScalef(1.f, 4.4f, 1.f);
	glutSolidCube(1);
	glPopMatrix();

	glColor3f(0.f, 0.f, 1.f);			//Left leg
	glPushMatrix();
	glTranslatef(0.8, 4, 0);
	glRotatef(personCycleAngle, 1, 0, 0);
	glTranslatef(-0.8, -4, 0);
	glTranslatef(0.8, 2.2, 0);
	glScalef(1.f, 4.4f, 1.f);
	glutSolidCube(1);
	glPopMatrix();

	glColor3f(0.f, 0.f, 1.f);			//Right arm
	glPushMatrix();
	glTranslatef(-2, 6.5, 0);
	glRotatef(personCycleAngle, 1, 0, 0);
	glTranslatef(2, -6.5, 0);
	glTranslatef(-2, 5, 0);
	glScalef(1.f, 4.f, 1.f);
	glutSolidCube(1);
	glPopMatrix();

	glColor3f(0.f, 0.f, 1.f);			//Left arm
	glPushMatrix();
	glTranslatef(2, 6.5, 0);
	glRotatef(-personCycleAngle, 1, 0, 0);
	glTranslatef(-2, -6.5, 0);
	glTranslatef(2, 5, 0);
	glScalef(1.f, 4.f, 1.f);
	glutSolidCube(1);
	glPopMatrix();
}

//------- Building Roof ------------------------------------------------

void roof(float roof_angle, float roof_width_x, float roof_width_z)
{
	float toRad = 3.14159265 / 180.0;
	float roof_pos_z = roof_width_z * cos(roof_angle * toRad) / 2;
	glPushMatrix();
	glTranslatef(0, 0, roof_pos_z);
	glRotatef(roof_angle, 1, 0, 0);
	glScalef(roof_width_x, 0.5, roof_width_z);
	glutSolidCube(1.0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0, 0, -roof_pos_z);
	glRotatef(-roof_angle, 1, 0, 0);
	glScalef(roof_width_x, 0.5, roof_width_z);
	glutSolidCube(1.0);
	glPopMatrix();
}

//------- Train Station ------------------------------------------------
void station()
{

	//base
	glColor4f(0.45, 0.45, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0.0, 1.5, 0);
	glScalef(55.0, 3.0, 35.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//building
	glColor4f(0.7, 0.45, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0.0, 10.5, 7.5);
	glScalef(55.0, 15.0, 20.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//Roof
	glColor4f(0.7, 0.45, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0.0, 18.5, -12.5);
	glScalef(55.0, 1.0, 60);
	glutSolidCube(1.0);
	glPopMatrix();

	glColor4f(0.4, 0.45, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0.0, 20.0, 7.5);
	roof(25, 60.0, 20.0);
	glPopMatrix();

	//post
	glPushMatrix();
	glTranslatef(26.0, 18.0, -40.5);
	glRotatef(90, 1, 0, 0);
	gluCylinder(q, 1.0, 1.0, 18.0, 20, 5);
	glPopMatrix();

	//post
	glPushMatrix();
	glTranslatef(-26.0, 18.0, -40.5);
	glRotatef(90, 1, 0, 0);
	gluCylinder(q, 1.0, 1.0, 18.0, 20, 5);
	glPopMatrix();

}

//------- Hotel --------------------------------------------------------
void hotel()
{

	//base
	glColor4f(0.45, 0.45, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0.0, 1.5, 0);
	glScalef(70.0, 3.0, 50.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//building
	float building_pos_z = 7.5;
	glColor4f(0.7, 0.45, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0.0, 20.25, building_pos_z);
	glScalef(65.0, 40.5, 35.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//balcony
	glColor4f(0.45, 0.45, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0.0, 21.5, 0);
	glScalef(64.5, 2.0, 50.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//roof
	glPushMatrix();
	glTranslatef(0.0, 42.0, building_pos_z);
	roof(25, 75.0, 35.0);
	glPopMatrix();


	//railing
	glColor4f(0.9, 0.9, 0.9, 1.0);
	glPushMatrix();
	glTranslatef(-31, 23, 0);
	glScalef(1.0, 3.0, 48.0);
	glutSolidCube(1.0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(31, 23, 0);
	glScalef(1.0, 3.0, 48.0);
	glutSolidCube(1.0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0, 23, -24);
	glScalef(63.0, 3.0, 1.0);
	glutSolidCube(1.0);
	glPopMatrix();

}

//------- Barrier Arm ---------------------------------------------------------
void arm()
{
	glColor4f(1.0, 1.0, 1.0, 1.0);
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, txId[0]);

	glBegin(GL_QUADS);
	glNormal3f(0.0, 0.0, 1.0); //Facing +z
	glTexCoord2f(0., 0.); glVertex3f(0., 0., 0.);
	glTexCoord2f(10., 0.); glVertex3f(35., 0., 0.);
	glTexCoord2f(10., 1.);  glVertex3f(35., 2., 0.);
	glTexCoord2f(0., 1.); glVertex3f(0., 2., 0.);

	glNormal3f(0.0, 0.0, -1.0); //Facing -z
	glTexCoord2f(0., 0.); glVertex3f(0., 0., -1.);
	glTexCoord2f(10., 0.); glVertex3f(35., 0., -1.);
	glTexCoord2f(10., 1.); glVertex3f(35., 2., -1.);
	glTexCoord2f(0., 1.); glVertex3f(0., 2., -1.);


	glEnd();
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_QUADS);
	glBegin(GL_QUADS);
	glNormal3f(0.0, 1.0, 0.0);   //Facing +y
	glVertex3f(0., 2., 0.);
	glVertex3f(35., 2., 0.);
	glVertex3f(35., 2., -1.);
	glVertex3f(0., 2., -1.);

	glNormal3f(0.0, -1.0, 0.0);   //Facing -y
	glVertex3f(0., 0., 0.);
	glVertex3f(35., 0., 0.);
	glVertex3f(35., 0., -1.);
	glVertex3f(0., 0., -1.);

	glNormal3f(1.0, 0.0, 0.0);   //Facing +x
	glVertex3f(0., 0., -1.);
	glVertex3f(0., 0., 0.);
	glVertex3f(0., 2., 0.);
	glVertex3f(0., 2., -1.);

	glNormal3f(-1.0, 0.0, 0.0);   //Facing -x
	glVertex3f(35., 0., -1.);
	glVertex3f(35., 0., 0.);
	glVertex3f(35., 2., 0.);
	glVertex3f(35., 2., -1.);

	glEnd();
	glPopMatrix();

}

void barrier_arm()
{
	//Mount
	glColor4f(0.8, 0.8, 0.8, 1.0);
	glPushMatrix();
	glTranslatef(0, 5.0, 0.0);
	glScalef(5.0, 10.0, 5.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//Arm

	glColor4f(1.0, 0, 0, 1.0);
	glPushMatrix();
	glTranslatef(0.0, 8.0, -3.0);
	glRotatef(arms_theta, 0, 0, 1);
	glTranslatef(-5, 0.0, 0.5);
	arm();
	glPopMatrix();

}

//------- Signal Light --------------------------------------------------------

void signalLight()
{
	//base
	glColor4f(0.2, 0.2, 0.2, 1.0);
	glPushMatrix();
	glScalef(5.0, 1.0, 5.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//post
	glPushMatrix();
	glTranslatef(0, 17.0, 0);
	glRotatef(90, 1, 0, 0);
	gluCylinder(q, 1.0, 1.0, 16.0, 20, 5);
	glPopMatrix();

	//light mount
	glColor4f(0.3, 0.3, 0.3, 1.0);
	glPushMatrix();
	glTranslatef(0, 13, 0);
	glScalef(3.0, 5.0, 3.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//Red light
	if (isSignalRed)
	{
		glColor4f(1.0, 0, 0, 1.0);
	}
	else
	{
		glColor4f(0.5, 0.5, 0.5, 1.0);
	}

	glPushMatrix();
	glTranslatef(1.5, 12, 0);
	glutSolidSphere(0.7, 6, 6);
	glPopMatrix();

	//Green light
	if (isSignalRed)
	{

		glColor4f(0.5, 0.5, 0.5, 1.0);
	}
	else
	{
		glColor4f(0, 1, 0, 1.0);
	}

	glPushMatrix();
	glTranslatef(1.5, 14, 0);
	glutSolidSphere(0.7, 10, 10);
	glPopMatrix();

}

//------- Base of engine, wagons (including wheels) --------------------
void base()
{
	glColor4f(0.2, 0.2, 0.2, 1.0);   //The base is nothing but a scaled cube!
	glPushMatrix();
	glTranslatef(0.0, 4.0, 0.0);
	glScalef(20.0, 2.0, 10.0);     //Size 20x10 units, thickness 2 units.
	glutSolidCube(1.0);
	glPopMatrix();

	glPushMatrix();					//Connector between wagons
	glTranslatef(11.0, 4.0, 0.0);
	glutSolidCube(2.0);
	glPopMatrix();

	//Wheels
	glColor4f(0.5, 0., 0., 1.0);
	glPushMatrix();
	glTranslatef(-8.0, 2.0, 5.1);
	gluDisk(q, 0.0, 2.0, 20, 2);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(8.0, 2.0, 5.1);
	gluDisk(q, 0.0, 2.0, 20, 2);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(-8.0, 2.0, -5.1);
	glRotatef(180.0, 0., 1., 0.);
	gluDisk(q, 0.0, 2.0, 20, 2);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(8.0, 2.0, -5.1);
	glRotatef(180.0, 0., 1., 0.);
	gluDisk(q, 0.0, 2.0, 20, 2);
	glPopMatrix();
}

//-- Locomotive ------------------------------------------------
void engine()
{
	base();

	//Cab
	glColor4f(0.8, 0.8, 0.0, 1.0);
	glPushMatrix();
	glTranslatef(7.0, 8.5, 0.0);
	glScalef(6.0, 7.0, 10.0);
	glutSolidCube(1.0);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(6.0, 14.0, 0.0);
	glScalef(4.0, 4.0, 8.0);
	glutSolidCube(1.0);
	glPopMatrix();

	//Boiler
	glPushMatrix();
	glColor4f(0.5, 0., 0., 1.0);
	glTranslatef(4.0, 10.0, 0.0);
	glRotatef(-90.0, 0., 1., 0.);
	gluCylinder(q, 5.0, 5.0, 14.0, 20, 5);
	glTranslatef(0.0, 0.0, 14.0);
	gluDisk(q, 0.0, 5.0, 20, 3);
	glColor4f(1.0, 1.0, 0.0, 1.0);
	glTranslatef(0.0, 4.0, 0.2);
	gluDisk(q, 0.0, 1.0, 20, 2);  //headlight!
	glPopMatrix();

}


//--- A rail wagon ---------------------------------------------------

void cart()
{
	glColor4f(1.0, 1.0, 1.0, 1.0);
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, txId[1]);


	glBegin(GL_QUADS);
	glNormal3f(0.0, 0.0, 1.0); //Facing +z
	glTexCoord2f(0., 0.); glVertex3f(0., 0., 0.);
	glTexCoord2f(0.5, 0.); glVertex3f(18., 0., 0.);
	glTexCoord2f(0.5, 0.3398);  glVertex3f(18., 10., 0.);
	glTexCoord2f(0., 0.3398); glVertex3f(0., 10., 0.);

	glNormal3f(0.0, 0.0, -1.0); //Facing -z
	glTexCoord2f(0., 0.); glVertex3f(0., 0., -10.);
	glTexCoord2f(0.5, 0.); glVertex3f(18., 0., -10.);
	glTexCoord2f(0.5, 0.3398); glVertex3f(18., 10., -10.);
	glTexCoord2f(0., 0.3398); glVertex3f(0., 10., -10.);

	glNormal3f(0.0, 1.0, 0.0);   //Facing +y
	glTexCoord2f(0., 0.4492); glVertex3f(0., 10., 0.);
	glTexCoord2f(0.5, 0.4492); glVertex3f(18., 10., 0.);
	glTexCoord2f(0.5, 0.8281); glVertex3f(18., 10., -10.);
	glTexCoord2f(0., 0.8281);  glVertex3f(0., 10., -10.);

	glNormal3f(1.0, 0.0, 0.0);   //Facing +x
	glTexCoord2f(0.22, 0.); glVertex3f(0., 0., -10.);
	glTexCoord2f(0.42, 0.); glVertex3f(0., 0., 0.);
	glTexCoord2f(0.42, 0.3398); glVertex3f(0., 10., 0.);
	glTexCoord2f(0.22, 0.3398); glVertex3f(0., 10., -10.);

	glNormal3f(-1.0, 0.0, 0.0);   //Facing -x
	glTexCoord2f(0.22, 0.); glVertex3f(18., 0., -10.);
	glTexCoord2f(0.42, 0.); glVertex3f(18., 0., 0.);
	glTexCoord2f(0.42, 0.3398); glVertex3f(18., 10., 0.);
	glTexCoord2f(0.22, 0.3398); glVertex3f(18., 10., -10.);


	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

}

void wagon()
{
	base();

	glColor4f(0.0, 1.0, 1.0, 1.0);
	glPushMatrix();
	glTranslatef(-9.0, 5.0, 5.0);
	//glScalef(18.0, 10.0, 10.0);
	cart();
	glPopMatrix();
}
//------Generic Houses----------------------------------------------------
void house(float width, float height, float breadth)
{
	float toRad = 3.14159265 / 180.0;

	glColor4f(0.7, 0.45, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0, height / 2, 0);
	glScalef(width, height, breadth);
	glutSolidCube(1.0);
	glPopMatrix();

	glColor4f(0.35, 0.35, 0.45, 1.0);
	glPushMatrix();
	glTranslatef(0, height + (0.25 * (breadth / 1.4)) * tan(20 * toRad), 0);
	roof(20, width + 5, breadth / 1.4);
	glPopMatrix();


}

//-----------Skybox------------------------------------------------------
void skybox()
{	
	glEnable(GL_TEXTURE_2D);

	////////////////////// LEFT WALL ///////////////////////
	glBindTexture(GL_TEXTURE_2D, txId[2]);
	glBegin(GL_QUADS);
	glTexCoord2f(0., 0.); glVertex3f(-1000, 0, 1000);
	glTexCoord2f(1., 0.); glVertex3f(-1000, 0., -1000);
	glTexCoord2f(1., 1.); glVertex3f(-1000, 1000., -1000);
	glTexCoord2f(0., 1.); glVertex3f(-1000, 1000, 1000);
	glEnd();

	////////////////////// FRONT WALL ///////////////////////
	glBindTexture(GL_TEXTURE_2D, txId[3]);
	glBegin(GL_QUADS);
	glTexCoord2f(0., 0.); glVertex3f(-1000, 0, -1000);
	glTexCoord2f(1., 0.); glVertex3f(1000, 0., -1000);
	glTexCoord2f(1., 1.); glVertex3f(1000, 1000, -1000);
	glTexCoord2f(0., 1.); glVertex3f(-1000, 1000, -1000);
	glEnd();

	////////////////////// RIGHT WALL ///////////////////////
	glBindTexture(GL_TEXTURE_2D, txId[4]);
	glBegin(GL_QUADS);
	glTexCoord2f(0., 0.); glVertex3f(1000, 0, -1000);
	glTexCoord2f(1., 0.); glVertex3f(1000, 0, 1000);
	glTexCoord2f(1., 1.); glVertex3f(1000, 1000, 1000);
	glTexCoord2f(0., 1.); glVertex3f(1000, 1000, -1000);
	glEnd();


	////////////////////// REAR WALL ////////////////////////
	glBindTexture(GL_TEXTURE_2D, txId[5]);
	glBegin(GL_QUADS);
	glTexCoord2f(0., 0.); glVertex3f(1000, 0, 1000);
	glTexCoord2f(1., 0.); glVertex3f(-1000, 0, 1000);
	glTexCoord2f(1., 1.); glVertex3f(-1000, 1000, 1000);
	glTexCoord2f(0., 1.); glVertex3f(1000, 1000, 1000);
	glEnd();

	/////////////////////// TOP //////////////////////////
	glBindTexture(GL_TEXTURE_2D, txId[6]);
	glBegin(GL_QUADS);
	glTexCoord2f(0., 0.); glVertex3f(-1000, 1000, -1000);
	glTexCoord2f(1., 0.); glVertex3f(1000, 1000, -1000);
	glTexCoord2f(1., 1.); glVertex3f(1000, 1000, 1000);
	glTexCoord2f(0., 1.); glVertex3f(-1000, 1000, 1000);
	glEnd();

	/////////////////////// FLOOR //////////////////////////
	glBindTexture(GL_TEXTURE_2D, txId[7]);
	glBegin(GL_QUADS);	
	glTexCoord2f(0., 0.); glVertex3f(-1000, 0., 1000);
	glTexCoord2f(1., 0.); glVertex3f(1000, 0., 1000);
	glTexCoord2f(1., 1.); glVertex3f(1000, 0., -1000);
	glTexCoord2f(0., 1.); glVertex3f(-1000, 0., -1000);
	glEnd();

	glDisable(GL_TEXTURE_2D);	
}


