#include "timers.h"

#define FALSE 0
#define TRUE 1

// Angluar position of the train
float theta = 0;

//Variables for PersonTimer
//Direction of body parts motion
bool isForward = FALSE;
//Current angle of persons motion cycle
float personCycleAngle = 20.0;
//Current angular position of a person
float personAngularPos = 0;

//current facing angle of person in rectangle walk pattern
float facingAngle = 90;
//Step size of rectagle walk
float step = 0.25;
//Current position of person in rectagle walk

float person2PosX = 0;
float person2PosZ = 0;

//Default timer value
int timer_value = 40;
//Camera attached to front of train true/false
bool isCameraOnTrain = FALSE;

//Status of sginaling light
bool isSignalRed = FALSE;

int arms_theta = 50;
bool isIncreasing = 1;
bool trig_arms = 0;


/*Timer for dictating the motion of the train.*/
void myTimer(int value)
{

	timer_value = 40;

	if (theta >= 360)
	{
		theta = 0;
	}

	//Train waits at station for 4 seconds
	if (theta == 205)
	{
		timer_value = 4000;
		theta++;
	}
	//Slow train as it approuches station
	else if (theta >= 140 && theta < 240)
	{
		theta = theta + 0.5;

	}
	//Full speed of train
	else
	{
		theta++;
	}

	glutPostRedisplay();
	glutTimerFunc(timer_value, myTimer, 0);

}


/*Timmer for dictating the motion and position of a person moving in a
circle inside the train track area.*/
void personTimer1(int value)
{
	//increment of angular position
	personAngularPos = personAngularPos + 0.1;

	//motion of body parts
	if (isForward)
	{
		personCycleAngle++;
	}
	else
	{
		personCycleAngle--;
	}
	//reverse cycle direction at 20 degees
	if (personCycleAngle >= 20 || personCycleAngle <= -20)
	{
		isForward = !isForward;
	}

	glutPostRedisplay();
	glutTimerFunc(40, personTimer1, 0);
}


/*function for dictating the position of a person moving in a rectangular
pattern.
param: rectangle[] - Dimensions of rectangle, current position, current angle,
and step size.
{x, z, xSize, zSize, anglef, step}

return:			   the current position {x, z, anglef}.
*/
void rectangleWalk(float rectangle[], float* position)
{
	float x = rectangle[0];
	float z = rectangle[1];
	float xSize = rectangle[2];
	float zSize = rectangle[3];
	float anglef = rectangle[4];
	float step = rectangle[5];

	if (x <= 0 && z <= 0)
	{
		anglef = 90;
		x = x + step;
	}
	else if (x < xSize && z <= 0)
	{
		x = x + step;
	}
	else if (x >= xSize && z <= 0)
	{
		anglef = 180;
		z = z + step;
	}
	else if (x >= xSize && z < zSize)
	{
		z = z + step;
	}
	else if (x >= xSize && z >= zSize)
	{
		anglef = 270;
		x = x - step;
	}
	else if (x > 0 && z >= zSize)
	{
		x = x - step;
	}
	else if (x <= 0 && z >= zSize)
	{
		anglef = 0;
		z = z - step;
	}
	else if (x <= 0 && z > 0)
	{
		z = z - step;
	}

	position[0] = x;
	position[1] = z;
	position[2] = anglef;
}


/*Timer for dictating the motion of a person moving in a rectangular pattern*/
void personTimer2(int value)
{
	float position[3];
	float rectangle[6] = { person2PosX, person2PosZ, 45, 8, facingAngle, step };

	rectangleWalk(rectangle, position);

	person2PosX = position[0];
	person2PosZ = position[1];
	facingAngle = position[2];

	glutPostRedisplay();
	glutTimerFunc(40, personTimer2, 0);
}

/*Timmer for dictating the colour of the signalling lights between red and
green.
*/
void signalTimmer(int value)
{
	if (theta >= 110 && theta <= 120)
	{
		isSignalRed = 1;
		timer_value = 10000;
	}
	else
	{
		isSignalRed = 0;
	}

	glutPostRedisplay();
	glutTimerFunc(timer_value, signalTimmer, 0);
}


/*Timer for dictaing the motion of the barrier arms when a train is near*/
void arms_timer(int value)
{
	int wait = 60;

	if (theta >= 250 && arms_theta >= 50)
	{
		trig_arms = !trig_arms;
		isIncreasing = !isIncreasing;
	}

	if (trig_arms)
	{
		if (arms_theta <= 0)
		{
			wait = 3000;
			isIncreasing = !isIncreasing;
		}

		if (isIncreasing && arms_theta < 50)
		{
			arms_theta++;
		}
		else if (!isIncreasing)
		{
			arms_theta--;
		}
		else if (arms_theta >= 50)
		{
			trig_arms = !trig_arms;
		}
	}

	glutPostRedisplay();
	glutTimerFunc(wait, arms_timer, 0);
}
