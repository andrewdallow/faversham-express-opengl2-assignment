#ifndef MODELS_H
#define MODELS_H
#include <iostream>
#include <GL/freeglut.h>
#include <cmath>

#include "timers.h"
extern GLUquadric *q;
//Object Tectures
extern GLuint txId[8];

//Skybox Textures
extern GLuint sky[6];

//-- Ground Plane --------------------------------------------------------
void floor();

//------- Rail Track ----------------------------------------------------
// A single circular track of specified radius
void track(float radius);

//-------- Tracks  ----------------------------------------------------
void tracks();


//--Draws a character model constructed using GLUT object ------------------
void drawPerson();

//------- Building Roof ------------------------------------------------

void roof(float roof_angle, float roof_width_x, float roof_width_z);

//------- Train Station ------------------------------------------------
void station();

//------- Hotel --------------------------------------------------------
void hotel();

//------- Barrier Arm ---------------------------------------------------------
void arm();

void barrier_arm();

//------- Signal Light --------------------------------------------------------

void signalLight();

//------- Base of engine, wagons (including wheels) --------------------
void base();

//-- Locomotive ------------------------------------------------
void engine();

//--- A rail wagon ---------------------------------------------------

void cart();

void wagon();

void house(float width, float height, float breadth);

void skybox();

#endif
