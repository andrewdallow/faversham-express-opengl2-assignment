//  ========================================================================
//  COSC363: Computer Graphics (2015);  University of Canterbury.
//
//  FILE NAME: Train.cpp
//  ========================================================================

#include <iostream>
#include <cmath>
#include <GL/freeglut.h>
#include "loadBMP.h"

#include "models.h"
#include "timers.h"

using namespace std;
#define GL_CLAMP_TO_EDGE 0x812F //To get rid of seams between textures (skybox)

//Camera Positions
float cameraAngle = 10.0;
float cameraPosX = 0;
float cameraPosZ = -200;
float cam_hgt = 40;
float cameraDirX = 0.0;
float cameraDirY = 0;
float cameraDirZ = 0;

/*Load the Textures from BMP files*/
void loadTexture()
{
	//Barrier Arm Texture
	glGenTextures(8, txId);   
	glBindTexture(GL_TEXTURE_2D, txId[0]);  
	loadBMP("barrier-arm.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MODULATE);

	//Wagon Texture				
	glBindTexture(GL_TEXTURE_2D, txId[1]);		
	loadBMP("WagonTexture.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	//Skybox
	// *** left ***
	glBindTexture(GL_TEXTURE_2D, txId[2]);
	loadBMP("negx.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// *** front ***
	glBindTexture(GL_TEXTURE_2D, txId[3]);
	loadBMP("posz.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// *** right ***
	glBindTexture(GL_TEXTURE_2D, txId[4]);
	loadBMP("posx.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// *** back***
	glBindTexture(GL_TEXTURE_2D, txId[5]);
	loadBMP("negz.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// *** top ***
	glBindTexture(GL_TEXTURE_2D, txId[6]);
	loadBMP("posy.bmp");

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// *** down ***
	glBindTexture(GL_TEXTURE_2D, txId[7]);
	loadBMP("negy.bmp");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}



/*Display the train */
void train()
{
	float spotlgt_pos[] = { -10.0f, 14.0f, 0.0f, 1.0f };  //light1 position 
	float spotlgt_dir[] = { -1.0f, -1.0f, 0.0f };

	//Engine
	glPushMatrix();
	 glRotatef(theta, 0., 1., 0.);
	glTranslatef(0, 1.0, -120);

	glLightfv(GL_LIGHT1, GL_POSITION, spotlgt_pos);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spotlgt_dir);
	engine();

	glPopMatrix();
	//Wagon 1
	glPushMatrix();
	  glRotatef(theta - 10.5f, 0., 1., 0.);
	  glTranslatef(0, 1.0, -120);
	  wagon();
	glPopMatrix();
	//Wagon 2
	glPushMatrix();
	  glRotatef(theta - 21.0f, 0., 1., 0.);
	  glTranslatef(0, 1.0, -120);
	  wagon();
	glPopMatrix();
	//Wagon 3
	glPushMatrix();
	  glRotatef(theta - 31.5f, 0., 1., 0.);
	  glTranslatef(0, 1.0, -120);
	  wagon();
	glPopMatrix();

	//Wagon 4
	glPushMatrix();
	  glRotatef(theta - 42.0f, 0., 1., 0.);
	  glTranslatef(0, 1.0, -120);
	  wagon();
	glPopMatrix();
}

/*Set the camera position and look direction*/
void camera()
{
	float lgt_pos[] = { 0.0f, 500.0f, -1000.0f, 1.0f };  //light0 position 
	float lgt_dir[] = { 0.0f, 0.0f, 0.0f };

	if (!isCameraOnTrain)
	{
		gluLookAt(cameraPosX, cam_hgt, cameraPosZ, cameraDirX, cameraDirY,
			cameraDirZ, 0.0, 1.0, 0.0);
		glLightfv(GL_LIGHT0, GL_POSITION, lgt_pos);
		glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lgt_dir);

		glRotatef(cameraAngle, 0, 1, 0);
		glTranslatef(0, 0, 0);
	}
	else
	{
		gluLookAt(-8, 14, -5, -20, 10, 0, 0.0, 1.0, 0.0);
		glLightfv(GL_LIGHT0, GL_POSITION, lgt_pos);   //light position

		glTranslatef(0, 0, 120);
		glRotatef(-theta, 0., 1., 0.);
	}
}

/*Display a range of houses at various position aroun the train track*/
void houses()
{
	glPushMatrix();
	  glRotated(50, 0, 1, 0);
	  glTranslatef(150, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(40, 15, 20);
	glPopMatrix();

	glPushMatrix();
	  glRotated(50, 0, 1, 0);
	  glTranslatef(95, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(20, 15, 20);
	glPopMatrix();

	glPushMatrix();
	  glRotated(25, 0, 1, 0);
	  glTranslatef(95, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(30, 15, 20);
	glPopMatrix();

	glPushMatrix();
	  glRotated(110, 0, 1, 0);
	  glTranslatef(80, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(30, 15, 30);
	glPopMatrix();

	glPushMatrix();
	  glRotated(110, 0, 1, 0);
	  glTranslatef(150, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(60, 20, 30);
	glPopMatrix();

	glPushMatrix();
	  glRotated(140, 0, 1, 0);
	  glTranslatef(150, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(40, 20, 30);
	glPopMatrix();

	glPushMatrix();
	  glRotated(145, 0, 1, 0);
	  glTranslatef(80, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(40, 32.5, 30);
	glPopMatrix();

	glPushMatrix();
	  glRotated(175, 0, 1, 0);
	  glTranslatef(90, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(20, 12.5, 20);
	glPopMatrix();

	glPushMatrix();
	  glRotated(200, 0, 1, 0);
	  glTranslatef(90, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(20, 12.5, 20);
	glPopMatrix();

	glPushMatrix();
	  glRotated(220, 0, 1, 0);
	  glTranslatef(150, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(40, 12.5, 20);
	glPopMatrix();

	glPushMatrix();
	  glRotated(0, 0, 1, 0);
	  glTranslatef(150, 0, 0);
	  glRotated(90, 0, 1, 0);
	  house(20, 12.5, 20);
	glPopMatrix();
}

/*Display the animated people*/
void people()
{
	glPushMatrix();
	  glRotated(-personAngularPos, 0, 1, 0);
	  glTranslatef(60, 0, 0);
	  drawPerson();
	glPopMatrix();


	glPushMatrix();
	  glTranslatef(-22.5, 3.0, 130);
	  glTranslatef(person2PosX, 0, person2PosZ);
	  glRotated(facingAngle, 0, 1, 0);
	  drawPerson();
	glPopMatrix();
}

/*Display the Train station and Hotal buildings*/
void buildings()
{
	//Station
	glPushMatrix();
	  glTranslatef(0, 0, 144);
	  station();
	glPopMatrix();

	//Hotel
	glPushMatrix();
	  glRotated(50, 0, 1, 0);
	  glTranslatef(0, 0, 155);
	  hotel();
	glPopMatrix();

}

/*Display the two animated barrier arms*/
void barrier_arms()
{
	//barrier arm
	glPushMatrix();
	  glTranslatef(0, 0.0, -130);
	  barrier_arm();
	glPopMatrix();

	//barrier arm
	glPushMatrix();
	  glRotatef(-20, 0, 1, 0);
	  glTranslatef(0, 0.0, -109);
	  glScalef(-1.0, 1.0, -1.0);
	  barrier_arm();
	glPopMatrix();
}

/* Display the two signal lights*/
void signalingLights()
{
	//Signaling Light
	glPushMatrix();
	  glRotatef(140, 0, 1, 0);
	  glTranslatef(0, 1.0, -110);
	  signalLight();
	glPopMatrix();

	//Signaling Light
	glPushMatrix();
	  glRotatef(225, 0, 1, 0);
	  glTranslatef(0, 1.0, -110);
	  signalLight();
	glPopMatrix();
}

//---------------------------------------------------------------------
void initialize(void)
{
	float grey[4] = { 0.2f, 0.2f, 0.2f, 1.0f };
	float white[4] = { 1.0f, 1.0f, 1.0f, 1.0f };

	q = gluNewQuadric();
	loadTexture();
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT0, GL_AMBIENT, grey);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, white);
	glLightfv(GL_LIGHT0, GL_SPECULAR, white);

	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT1, GL_AMBIENT, grey);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, white);
	glLightfv(GL_LIGHT1, GL_SPECULAR, white);
	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 30.0);
	glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 0.01f);

	glMaterialfv(GL_FRONT, GL_SPECULAR, white);
	glMaterialf(GL_FRONT, GL_SHININESS, 50);

	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	gluQuadricDrawStyle(q, GLU_FILL);
	glClearColor(0.0, 0.0, 0.0, 0.0);  //Background colour

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(75., 1.0, 5.0, 2000.0);   //Perspective projection
}
//-------------------------------------------------------------------
void display(void)
{
   glClear (GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   camera();

   glPushMatrix();
     glTranslatef(0, -500, 0);
     skybox();
   glPopMatrix();
   
   floor();
   tracks(); 

   train();   
   
   buildings();
   barrier_arms();
   signalingLights();
  
   people();   

   houses();  

   glutSwapBuffers();   
}


// To enable the use of left and right arrow keys to rotate the scene
void special(int key, int x, int y)
{
	if (key == GLUT_KEY_LEFT) cameraAngle++;
	else if (key == GLUT_KEY_RIGHT) cameraAngle--;
	else if (key == GLUT_KEY_UP) cam_hgt++;
	else if (key == GLUT_KEY_DOWN) cam_hgt--;
	
	glutPostRedisplay();
	//Drivers View
	if (key == GLUT_KEY_F1) 
	{
		isCameraOnTrain = !isCameraOnTrain;
		cameraAngle = 10.0;
	}
	//Barrier arms View
	else if (key == GLUT_KEY_F2)
	{
		if (isCameraOnTrain)
		{
			isCameraOnTrain = !isCameraOnTrain;
			cameraAngle = 10.0;
		}
		//camera position
		cameraPosX = 0.0;
		cam_hgt = 40;
		cameraPosZ = -200;
		//camera direction
		cameraDirX = 0.0;
		cameraDirY = 0.0;
		cameraDirZ = 0.0;
		cameraAngle = 10.0;

	}
	//Platform View
	else if (key == GLUT_KEY_F3)
	{
		if (isCameraOnTrain)
		{
			isCameraOnTrain = !isCameraOnTrain;
		}
		//camera position
		cameraPosX = 30;
		cam_hgt = 12.0;
		cameraPosZ = 136.0;
		//camera direction
		cameraDirX = -200.0;
		cameraDirY = 5;
		cameraDirZ = 0.0;
		cameraAngle = 10.0;
	}
	//Hotel Balcony View
	else if (key == GLUT_KEY_F4)
	{
		if (isCameraOnTrain)
		{
			isCameraOnTrain = !isCameraOnTrain;
		}
		//camera position
		cameraPosX = 120;
		cam_hgt = 30;
		cameraPosZ = 68;
		//camera direction
		cameraDirX = 70;
		cameraDirY = 5;
		cameraDirZ = 0.0;
		cameraAngle = 10.0;
	}
	///Birds-eye View
	else if (key == GLUT_KEY_F5)
	{
		if (isCameraOnTrain)
		{
			isCameraOnTrain = !isCameraOnTrain;
		}
		//camera position
		cameraPosX = 1;
		cam_hgt = 250;
		cameraPosZ = 1;
		//camera direction
		cameraDirX = 0;
		cameraDirY = 0;
		cameraDirZ = 0;
		cameraAngle = 10.0;

	}
	//Station View
	else if (key == GLUT_KEY_F6)
	{
		if (isCameraOnTrain)
		{
			isCameraOnTrain = !isCameraOnTrain;
		}
		//camera position
		cameraPosX = -100;
		cam_hgt = 30;
		cameraPosZ = 100;
		//camera direction
		cameraDirX = 20;
		cameraDirY = 5;
		cameraDirZ = 90;
		cameraAngle = 10.0;

	}
}

//---------------------------------------------------------------------
int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_DOUBLE|GLUT_DEPTH);
   glutInitWindowSize (600, 600); 
   glutInitWindowPosition (50, 50);
   glutCreateWindow ("Toy Train");
   initialize ();

   glutDisplayFunc(display);
   glutSpecialFunc(special); 
   glutTimerFunc(timer_value, signalTimmer, 0);
   glutTimerFunc(timer_value, myTimer, 0);
   glutTimerFunc(timer_value, arms_timer, 0); 
   glutTimerFunc(40, personTimer1, 0);
   glutTimerFunc(40, personTimer2, 0);
   
   glutMainLoop();
   return 0;
}
