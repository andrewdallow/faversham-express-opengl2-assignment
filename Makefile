# File:   Makefile
# Author: A. T. Dallow
# Date:   01 Apr 2015
# Descr:  Makefile for Train
CC=g++
# Hey!, I am comment number 2. I want to say that CFLAGS will be the
# options I'll pass to the compiler.
CFLAGS=-Wall -Werror -lGLU -lGL -lglut

all: train

train: train.o timers.o models.o
	$(CC) $(CFLAGS) train.o timers.o models.o -o train

train.o: train.cpp models.h timers.h loadBMP.h
	$(CC) -c $(CFLAGS) Train.cpp

Timers.o: timers.cpp timers.h
	$(CC) -c $(CFLAGS) Timers.cpp

models.o: models.cpp models.h
	$(CC) -c $(CFLAGS) models.cpp

clean:
	rm *o

